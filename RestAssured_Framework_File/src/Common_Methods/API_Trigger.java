package Common_Methods;

import Repositories.Environment;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {
	
	public static Response post_api(String headername,String headervalue,String req_body,String endpoint) {
		
		RequestSpecification request = RestAssured.given();
		request.header(headername,headervalue);
		request.body(req_body);
		Response response = request.post(endpoint);
		return response;
		
	}

	public static Response patch_api(String headervalue, String headername, String req_body, String endpoint) {

		RequestSpecification request = RestAssured.given();
		request.header(headername, headervalue);
		request.body(req_body);
		Response response = request.patch(Environment.hostname() + Environment.patch_resource());
		return response;
	}
	
	public static Response get_api(String headervalue,String headername,String req_body,String endpoint) {
		RequestSpecification request = RestAssured.given();
		request.header(headername,headervalue);
		request.body(req_body);
		Response response = request.get(Environment.hostname()+Environment.get_resource());
		return response;
	}

	public static Response put_api(String headername, String headervalue, String req_body, String endpoint) {

		RequestSpecification request = RestAssured.given();
		request.header(headername, headervalue);
		request.body(req_body);
		Response response = request.put(Environment.hostname() + Environment.put_resource());
		return response;
	}

	public static Response Delete_api(String headername, String headervalue, String req_body, String Endpoint) {
		RequestSpecification request = RestAssured.given();
		request.headers(headername, headervalue);
		request.body(req_body);
		Response response = request.delete(Environment.hostname() + Environment.Delete_resource());
		return response;
	}
}
