package Common_Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class utility {

	public static ArrayList<String> readexceldata(String Sheetname, String Testcase) throws IOException {

		ArrayList<String> arraydata = new ArrayList<String>();

		String projectdir = System.getProperty("user.dir");
		System.out.println("Current Directory Project is" + projectdir);

		FileInputStream fls = new FileInputStream(projectdir + "\\Data_Files\\Input_Data.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fls);
		int count = wb.getNumberOfSheets();
		System.out.println("count of the sheet:" + count);

		for (int i = 0; i < count; i++) {

			if (wb.getSheetName(i).equals(Sheetname)) {
				System.out.println("Sheet at index" + " " + i + ":" + wb.getSheetName(i));

				XSSFSheet datasheet = wb.getSheetAt(i);
				Iterator<Row> rows = datasheet.iterator();
				while (rows.hasNext()) {
					Row datarows = rows.next();
					String testname = datarows.getCell(0).getStringCellValue();
					if (testname.equals(Testcase)) {
						Iterator<Cell> cellvalue = datarows.cellIterator();
						while (cellvalue.hasNext()) {
							String testdata = cellvalue.next().getStringCellValue();
							System.out.println(testdata);
							arraydata.add(testdata);

						}
					} else {
						System.out.println(Testcase + " Testcase not found in the sheet :" + wb.getSheetName(i));
					}

				}
				break;
			} else {
				System.out.println(Sheetname + " " + "Sheetname not found in the file:" + i);
			}

		}
		wb.close();
		return arraydata;
	}

	public static void evidencefile(String Filename, File Filelocation, String Requestbody, String Responsebody,
			String Endpoint, String date) throws IOException {
		// step 1 open the file
		File newtextfile = new File(Filelocation + "\\" + Filename);
		System.out.println(newtextfile.getName());

		// step 2 write data in the file
		FileWriter Writedata = new FileWriter(newtextfile);
		Writedata.write("Requestbody is : " + Requestbody + "\n\n");
		Writedata.write("Responsebody is : " + Responsebody + "\n\n");
		Writedata.write("Endpoint is : " + Endpoint + "\n\n");
		Writedata.write("Current date : " + date);

		// step 3 close the file
		Writedata.close();
	}

	public static File createdirectory(String dirname) {

		String projectdir = System.getProperty("user.dir");
		System.out.println("Currentdirectory project" + projectdir);

		File patchapi = new File(projectdir + "\\" + dirname);
		if (patchapi.exists()) {
			System.out.println(patchapi + "already exists");
		} else {
			System.out.println(patchapi + "file has to create");
		}
		patchapi.mkdir();
		return patchapi;
	}

}
