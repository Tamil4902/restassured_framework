package Test_case;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.utility;
import Repositories.Environment;
import Repositories.Request_body;
import common_package.API_trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Post_api extends Environment {

	public static void execute() throws IOException {
		
		File dirnmae = utility.createdirectory("post_api");
		
		String requestbody = Request_body.Post_api();
		
		String Endpoint = Environment.hostname()+Environment.post_resource();
		
		
		
		Response response = API_trigger.Post_api(Environment.headername(),Environment.headervalue(),requestbody, Endpoint);
		
		utility.evidencefile("post_api", dirnmae,requestbody,response.getBody().asString(), Endpoint, response.getHeader("date"));
	
	    System.out.println(response.statusCode());
	    System.out.println(response.getBody().asString());
	    System.out.println(response.getBody().jsonPath().getString("name"));
	    System.out.println(response.getBody().jsonPath().getString("job"));
	    System.out.println(response.getBody().jsonPath().getString("id"));
	    System.out.println(response.getBody().jsonPath().getString("createdAt").substring(0,10));
	    
	    int statuscode = response.statusCode();
	    String res_name = response.getBody().jsonPath().getString("name");
	    String res_job = response.getBody().jsonPath().getString("job");
	    String res_id = response.getBody().jsonPath().getString("id");
	    String res_createdAt = response.getBody().jsonPath().getString("createdAt").substring(0,10);
	    
	    JsonPath req_body = new JsonPath(requestbody);
	    String req_name = req_body.getString("name");
	    String req_job = req_body.getString("job");
	    
	    LocalDateTime currentdate = LocalDateTime.now();
	    String expecteddate = currentdate.toString().substring(0,10);
	    
	    Assert.assertEquals(statuscode, 201);
	    Assert.assertEquals(req_name, res_name);
	    Assert.assertEquals(req_job,res_job);
	    Assert.assertNotNull(res_id);
	    Assert.assertEquals(expecteddate,res_createdAt);
	
	
	
	}

}
