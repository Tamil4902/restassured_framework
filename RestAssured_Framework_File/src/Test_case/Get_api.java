package Test_case;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.utility;
import Repositories.Environment;
import Repositories.Request_body;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Get_api {

	public static void execute() throws IOException {
		
		File dirname = utility.createdirectory("get_api");

		String Endpoint = Environment.hostname() + Environment.get_resource();
		Response response = API_Trigger.get_api(Environment.headervalue(), Environment.headervalue(),
				Request_body.get_api(), Endpoint);
		
		utility.evidencefile("Get_api", dirname,Request_body.get_api(),response.getBody().asPrettyString(), Endpoint,response.getHeader("date"));

		String res_body = response.getBody().asPrettyString();
		System.out.println(res_body);
		int statuscode = response.statusCode();
		System.out.println(statuscode);

		JsonPath res_jsn = new JsonPath(res_body);

		int count = res_jsn.getInt("data.size()");
		System.out.println(count);
		int id = 0;
		int idArr[] = new int[count];
		int ids[] = { 7, 8, 9, 10, 11, 12 };

		for (int i = 0; i < count; i++) {

			id = res_jsn.getInt("data[" + i + "].id");
			System.out.println(id);
			idArr[i] = id;

			String res_firstName = res_jsn.getString("data[" + i + "].first_name");
			System.out.println(res_firstName);

			String res_lastName = res_jsn.getString("data[" + i + "].last_name");
			System.out.println(res_lastName);

			String res_email = res_jsn.getString("data[" + i + "].email");
			System.out.println(res_email);
		}
		for (int i = 0; i < idArr.length; i++) {
			System.out.println(idArr[i]);

			Assert.assertEquals(ids[i], idArr[i]);

		}

	}

}
