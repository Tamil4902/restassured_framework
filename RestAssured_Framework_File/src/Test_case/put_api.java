package Test_case;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.utility;
import Repositories.Environment;
import Repositories.Request_body;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class put_api {

	public static void execute() throws IOException {
		
		
		File dirname = utility.createdirectory("put_api");
		
		
		String Endpoint = Environment.hostname()+Environment.put_resource();
		Response response = API_Trigger.put_api(Environment.headername(),Environment.headervalue(),Request_body.put_api(), Endpoint);
		
		utility.evidencefile("put_api", dirname,Request_body.put_api(),response.getBody().asString(), Endpoint,response.getHeader("Date"));
		
		System.out.println(response.statusCode());
		System.out.println(response.getBody().asPrettyString());
		System.out.println(response.getBody().jsonPath().getString("name"));
		System.out.println(response.getBody().jsonPath().getString("job"));
		System.out.println(response.getBody().jsonPath().getString("updatedAt").substring(0,10));
		
		String res_name = response.getBody().jsonPath().getString("name");
		String res_job = response.getBody().jsonPath().getString("job");
		String res_updatedAt = response.getBody().jsonPath().getString("updatedAt").substring(0,10); 
	
	     JsonPath req_body = new JsonPath(Request_body.put_api());
	     String req_name = req_body.getString("name");
	     String req_job = req_body.getString("job");
	     
	     LocalDateTime currenttime = LocalDateTime.now();
	     String expectedtime = currenttime.toString().substring(0,10);
	     
	     Assert.assertEquals(response.statusCode(),200);
	     Assert.assertEquals(req_name,res_name);
	     Assert.assertEquals(req_job,res_job);
	     Assert.assertEquals(expectedtime,res_updatedAt);
	
	
	
	}

}
