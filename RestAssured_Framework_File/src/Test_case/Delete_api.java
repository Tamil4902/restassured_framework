package Test_case;



import java.io.File;
import java.io.IOException;

import Common_Methods.API_Trigger;
import Common_Methods.utility;
import Repositories.Environment;
import Repositories.Request_body;
import io.restassured.response.Response;

public class Delete_api {

	public static void execute() throws IOException {
		 File dirname = utility.createdirectory("Delete_api");
		
		String Endpoint = Environment.hostname()+Environment.Delete_resource();
		
		Response response = API_Trigger.Delete_api(Environment.headername(),Environment.headervalue(),Request_body.Delete_api(),Endpoint);
		
		utility.evidencefile("Delete_api", dirname,Request_body.Delete_api(),response.getBody().asString(), Endpoint,response.getHeader("date"));
		
		System.out.println(response.statusCode());
		System.out.println(response.getBody().asString());
		
		
		
	
		

	}
}
