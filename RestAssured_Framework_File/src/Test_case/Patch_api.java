package Test_case;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.utility;
import Repositories.Environment;
import Repositories.Request_body;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Patch_api {

	public static void execute() throws IOException {

		File dirname = utility.createdirectory("patchapi");

		String Endpoint = Environment.hostname() + Environment.patch_resource();

		Response response = API_Trigger.patch_api(Environment.headervalue(), Environment.headername(),
				Request_body.patch_api(), Endpoint);

		utility.evidencefile("patchapi", dirname, Request_body.patch_api(), response.getBody().asString(), Endpoint,
				response.getHeader("date"));

		System.out.println(response.getBody().asString());
		System.out.println(response.statusCode());
		System.out.println(response.getBody().jsonPath().getString("name"));
		System.out.println(response.getBody().jsonPath().getString("job"));
		System.out.println(response.getBody().jsonPath().getString("updatedAt").substring(0, 10));

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 10);

		JsonPath jsp_req = new JsonPath(Request_body.patch_api());

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		Assert.assertEquals(req_name, response.getBody().jsonPath().getString("name"));
		Assert.assertEquals(req_job, response.getBody().jsonPath().getString("job"));
		Assert.assertEquals(expecteddate, response.getBody().jsonPath().getString("updatedAt").substring(0, 10));

	}

}
