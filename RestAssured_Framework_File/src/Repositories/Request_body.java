package Repositories;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.utility;

public class Request_body extends Environment {

	public static String Post_api() throws IOException {
		ArrayList<String> data = utility.readexceldata("POST_API", "test_case_1");
		String key_name = data.get(1);
		String value_name = data.get(2);
		String key_job = data.get(3);
		String value_job = data.get(4);
		System.out.println(data);
		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";
		return req_body;
	}
	
	public static String get_api() {
		String req_body = "";
		return req_body;
		
	}

	public static String patch_api() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";

		return req_body;
	}

	public static String put_api() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";

		return req_body;
	}

	public static String Delete_api() {
		String req_body = "";

		return req_body;
	}
}
