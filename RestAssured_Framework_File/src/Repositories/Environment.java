package Repositories;

public class Environment  {

	public static String hostname() {
		String Hostname = "https://reqres.in/";
		return Hostname;
	}
	public static String post_resource() {
		String Resource = "api/users";
		return Resource;
	}
	public static String get_resource() {
		String Resource = "api/users?page=2";
		return Resource;
	}

	public static String patch_resource() {
		String Resource = "api/users/2";
		return Resource;
	}
	
	public static String put_resource() {
		String Resource = "api/users/2";
		return Resource;
	}
	public static String Delete_resource() {
		String Resource = "api/users/2";
		return Resource;
	}

	public static String headername() {
		String Headername = "Content-Type";
		return Headername;
	}

	public static String headervalue() {
		String Headervalue = "application/json";
		return Headervalue;
	}

}
