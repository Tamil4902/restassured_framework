package Runner_api;

import java.io.IOException;


import Test_case.Delete_api;
import Test_case.Get_api;
import Test_case.Patch_api;
import Test_case.Post_api;
import Test_case.put_api;

public class API_runner {

	public static void main(String[] args) throws IOException {
	
		    Post_api.execute();
		    Get_api.execute();
		    Patch_api.execute();
		    put_api.execute();
		    Delete_api.execute();

	}

}
