##RestAssured_framework

Creating the Framework with encapsulate all the below requirements:-

1.configure the REST API:-
  *Execute

  *Extrace Response (RestAssured library)

  *Parse the Resposne (Using Jsonpath in RestAssured library)

  *validate the Response(using TestNG library )

  *Read the data from Excel(Using ApachePOI)

2.Framework is able to reusable by Creating the common methods and centralized configuration.In this framework we Creating the Repostries package with consists of requestbody class and Environment Class

  *Environment Class contains common data like Header name,Header Value, Hostname and Different Resource of Rest API 
  *RequestBody class contains all the request bodies of different rest api methods

3.In framework we are Creating the Common method package it contains a API Trigger class and utility class
  *API Trigger class contains all trigger methods of our REST API's.
  *utility class contains CreateLogDirectory creation ,Evidence file creation and Read the File from Excel

4.Data Driven test is supports in the framework.   

5.In Framework,we are creating the Separate Testcases of each and every API's.

6.Hence created a Runner package that consist of Runner_API class for run all the REST API's at a single time.


